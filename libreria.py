import textblob as tb
from textblob import TextBlob
import re
from re import search
import nltk
import spacy
import textblob as tb
from lingua.language import Language
from lingua.builder import LanguageDetectorBuilder
import warnings
from googletrans import Translator
warnings.filterwarnings('ignore')
import pandas as pd
from nltk.corpus import stopwords

class Libreria():

    nlp = spacy.load('en_core_web_lg')
    stop_words = set(stopwords.words('english'))

    def __init__(self,url_campo,url_traductor):
        self.url_campo = url_campo
        url_campo = pd.read_csv(url_campo)
        self.url_campo_list=url_campo['word'].tolist()
        palabras_dic_campo= { stu :1 for stu in self.url_campo_list }
        tb.en.spelling.update(palabras_dic_campo)

        self.url_traductor = url_traductor
        url_traductor = pd.read_csv(url_traductor)
        self.url_traductor_list=url_traductor['Palabras'].tolist()

        self.vocabulario_lista = self.url_traductor_list + self.url_campo_list
     
    def lowercase(data):
        res = any(ele.isupper() for ele in data) # mayuscula
        if res == True:
            return 0
        return 1

    def space(data):
        res = any(ele.isspace() for ele in data)
        if res == True:
            return 0
        return 1
    
    def underscored_starendwith(data): # valida si el campo parte y termina con letras minusculas y sin numeros (que no parta con [_] ni contengan numeros)
        pattern = r'^[a-z0-9]+(_[a-z0-9]+){0,}$'
        if re.search(pattern, data):
            return(1)
        else:
            return(0)
    
    def brackets(data): # Valida si tiene () []
        pattern = r'([\(\[]).*?([\)\]])'
        if re.search(pattern, data):
            return(0)
        else:
            return(1)
    
    def remove_stopwords(data): # Eliminar stopword del campo | no se usa
        data = data.replace('_',' ')
        t = [word for word in data.split() if word not in (Libreria.stop_words)]
        data = '_'.join(t)    
        return data
    
    def validate_stopwords(data): # comprueba si tiene stopword o no | no se usa
        data_1 = data.replace('_',' ')
        t = [word for word in data_1.split() if word not in (Libreria.stop_words)]
        data_1 = '_'.join(t)
        if len(data_1) != len(data):
            return(0)
        else:
            return(1)   
    
    def is_lenght_greater_than_three(data):
        pattern = r'[A-Za-z]'          
        if len([x for x in data if re.search(pattern, x)]) <=3:
            return(0)
        else:
            return (1)
    
    def english_flag(self, data):
        data = data.lower()
        data = data.replace('_',' ')
        data= re.sub(r"\(|\)", "", data)
        data = re.sub(r"[0-9]", "", data)
        # data = re.sub(r"[0-9]", "", data)
        languages: list[Language] = [Language.ENGLISH, Language.SPANISH]
        detector = LanguageDetectorBuilder.from_languages(*languages).with_preloaded_language_models().build()
        data = data.split(' ')
        data_v = [word for word in data if word not in self.vocabulario_lista]
        data_join = ' '.join(data_v)
        detect = detector.detect_language_of(data_join)
        if detect == None or detect == Language.ENGLISH:
            return 1
        else: return 0

#hola mi nombre es fernando
#lol jajaja ejemplo fernando version 2

    def correct(data): # Corrección de palabras | no se usa
        data = data.replace('_',' ')
        data= re.sub(r"\(|\)", "", data)
        out = ''.join(TextBlob(data).correct())
        final = out.replace(' ','_')
        return final.lower()
    
    def written_word_ok(data): # Corrección de palabras | no se usa
        data2 = data.replace('_',' ')
        out = ''.join(TextBlob(data2).correct())
        final = out.replace(' ','_')
        if data2 != out:
            return(0)
        else: return(1)    

    def traslate(data, trasla = True): # no se usa
        translator = Translator()
        data = Libreria.remove_stopwords(data)
        data = data.replace('_',' ')
        data= re.sub(r"\(|\)", "", data)
        token = data.split(' ')
        if trasla == True:
            token = [translator.translate(word, src='auto', dest='en').text for word in token]
        return '_'.join(token).lower()

    def validator_final_ok(self, data):
        if data in self.vocabulario_lista:
            return 1 # que retorne '1' si esta en esa lista
        elif (Libreria.lowercase(data) == 1 and Libreria.space(data) == 1 and Libreria.underscored_starendwith(data) == 1 and Libreria.brackets(data) == 1 and Libreria.is_lenght_greater_than_three(data) == 1\
            and Libreria.english_flag(self, data) == 1):
            return (1)
        else:  return (0)
